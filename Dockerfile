from python:3.9.0-slim

RUN apt-get update && apt-get install -y build-essential gcc libpq-dev

RUN mkdir /flask_backend
WORKDIR /flask_backend 
COPY flask_backend/requirements.txt /flask_backend/requirements.txt
RUN python -m venv /flask_backend/env
ENV PATH="/flask_backend/env/bin:$PATH"
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY flask_backend /flask_backend