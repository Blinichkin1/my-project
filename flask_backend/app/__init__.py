from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from prometheus_flask_exporter import PrometheusMetrics

from config import config


db = SQLAlchemy()
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
metrics = PrometheusMetrics.for_app_factory()


def create_app(config_name):
	app = Flask(__name__)
	app.config.from_object(config[config_name])

	config[config_name].init_app(app)
	db.init_app(app)
	login_manager.init_app(app)
	metrics.init_app(app)

	from .blueprints.main import main as main_blueprint
	app.register_blueprint(main_blueprint)

	from .blueprints.auth import auth as auth_blueprint
	app.register_blueprint(auth_blueprint, url_prefix='/auth')

	from .blueprints.devices import devices as devices_blueprint
	app.register_blueprint(devices_blueprint, url_prefix='/devices')

	return app
