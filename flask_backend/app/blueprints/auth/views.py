from flask import request, redirect, render_template, flash, url_for
from flask_login import login_user, logout_user

from . import auth
from app import db
from app.models.user import User


@auth.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('auth/register.html')

    data = request.get_json()

    user = User()
    user.email = data['email'].lower()
    user.password = data['password']
    user.last_name = data['last_name']
    user.first_name = data['first_name']

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('auth.login'))


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('auth/login.html')

    data = request.get_json(force=True, silent=True)
    user = User.query.filter_by(email=data['email'].lower()).first()

    if user is not None and user.verify_password(data['password']):
        login_user(user, data['remember_me'])
        temp = request.args.get('next')
        if temp is None or not temp.startswith('/'):
            temp = url_for('main.index')
        return redirect(temp)
    flash('Invalid email or password.')


@auth.route('/logout')
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main.index'))
