import prometheus_client
from flask import render_template, Response

from . import main


@main.route('/')
def index():
    return render_template("index.html")


@main.route('/metrics')
def metrics():
    CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')
    return Response(prometheus_client.generate_latest(), mimetype=CONTENT_TYPE_LATEST)

# @main.route('/<path:path>', methods=['GET'])
# def any_root_path(path):
# 	return render_template('index.html')
