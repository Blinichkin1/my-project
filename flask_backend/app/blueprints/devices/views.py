from chirpstack_api.as_pb.external import api
from flask import current_app, render_template, request
import grpc

from . import devices


@devices.route('/')
def get_devices():
    channel = grpc.insecure_channel(current_app.config['SERVER'])
    client = api.DeviceServiceStub(channel)

    req = api.ListDeviceRequest()
    req.application_id = 1
    req.limit = 10

    resp = client.List(req, metadata=current_app.config['AUTH_TOKEN'])

    return render_template('devices/list.html', resp=resp)


@devices.route('/create', methods=['GET', 'POST'])
def create_device():
    if request.method == 'GET':
        return render_template('devices/create.html')

    data = request.get_json(force=True, silent=True)

    channel = grpc.insecure_channel(current_app.config['SERVER'])
    client = api.DeviceServiceStub(channel)

    req = api.CreateDeviceRequest()
    req.device.application_id = 1
    req.device.name = data['name']
    req.device.description = data['description']
    req.device.device_profile_id = data['device_profile_id']  # "080cd631-bfbe-41d9-9ff1-9c250e217d20"
    req.device.is_disabled = data['is_disabled']
    req.device.skip_f_cnt_check = data['skip_f_cnt_check']
    req.device.dev_eui = data['dev_eui']  # "1111111111111111"

    client.Create(req, metadata=current_app.config['AUTH_TOKEN'])

    req = api.CreateDeviceKeysRequest()
    req.device_keys.dev_eui = data['dev_eui']
    req.device_keys.nwk_key = data['nwk_key']

    client.CreateKeys(req, metadata=current_app.config['AUTH_TOKEN'])

    return "device has been created", 200


@devices.route('/edit/<dev_eui>', methods=['GET', 'POST'])
def edit_device(dev_eui):
    channel = grpc.insecure_channel(current_app.config['SERVER'])
    client = api.DeviceServiceStub(channel)
    req = api.CreateDeviceRequest()

    if request.method == 'GET':
        req = api.GetDeviceRequest()
        req.dev_eui = dev_eui

        resp = client.Get(req, metadata=current_app.config['AUTH_TOKEN'])

        req = api.GetDeviceKeysRequest()
        req.dev_eui = dev_eui

        key = client.GetKeys(req, metadata=current_app.config['AUTH_TOKEN'])

        return render_template('devices/edit.html', resp=resp, key=key)

    data = request.get_json(force=True, silent=True)

    req.device.application_id = 1
    req.device.name = data['name']
    req.device.description = data['description']
    req.device.device_profile_id = data['device_profile_id']
    req.device.is_disabled = data['is_disabled']
    req.device.skip_f_cnt_check = data['skip_f_cnt_check']
    req.device.dev_eui = data['dev_eui']

    client.Update(req, metadata=current_app.config['AUTH_TOKEN'])

    req = api.UpdateDeviceKeysRequest()
    req.device_keys.dev_eui = data['dev_eui']
    req.device_keys.nwk_key = data['nwk_key']

    client.UpdateKeys(req, metadata=current_app.config['AUTH_TOKEN'])

    return "Device has been updated", 200


@devices.route('/delete', methods=['POST'])
def delete_device():
    data = request.get_json(force=True, silent=True)
    channel = grpc.insecure_channel(current_app.config['SERVER'])
    client = api.DeviceServiceStub(channel)

    req = api.DeleteDeviceRequest()
    req.dev_eui = data['dev_eui']
    client.Delete(req, metadata=current_app.config['AUTH_TOKEN'])

    return "Device has been deleted", 200
