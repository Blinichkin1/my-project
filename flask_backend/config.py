import os


class Config:
    SERVER = "212.193.88.194:8080"
    api_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGlfa2V5X2lkIjoiMDY0NGQ5NjAtZDU0MS00NTllLWFlN2QtNGU0NGI1NGExYTQ2IiwiYXVkIjoiYXMiLCJpc3MiOiJhcyIsIm5iZiI6MTYwODg4ODY4OSwic3ViIjoiYXBpX2tleSJ9.UHbNFN7wX8PiGhLCZo8aZAah2Oks5D41kaOmd-n-fnI"
    AUTH_TOKEN = [("authorization", "Bearer %s" % api_token)]

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False
    SQLALCHEMY_DATABASE_URI = "postgresql://ADMIN:PASSWORD@postgresql/Test"

    MQTT_BROKER_URL = 'broker.hivemq.com'  # use the free broker from HIVEMQ
    MQTT_BROKER_PORT = 1883  # default port for non-tls connection
    MQTT_USERNAME = ''  # set the username here if you need authentication for the broker
    MQTT_PASSWORD = ''  # set the password here if the broker demands authentication
    MQTT_KEEPALIVE = 5  # set the time interval for sending a ping to the broker to 5 seconds
    MQTT_TLS_ENABLED = False  # set TLS to disabled for testing purposes

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    pass


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
